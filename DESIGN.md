# Design notes

This document lists a few details needed for designing the right APIs for the different Danbooru implementations.
Unfortunately most of these are laregly incompatible for syntax and URL paths.

## Konachan.{com,net} - yande.re

These are the boards originally supported by Danbooru Client.

Supported (doesn't list what Danbooru Client doesn't need at the moment):

- posts
- pools[^1]
- tags[^2]
- search by tags
- search by related tags

Return types:

- JSON[^3]
- XML

Structure of a post:

TODO

Structure of a tag:

TODO

Structure of a pool:

TODO


## Danbooru (danbooru.donmai.us)

The "original" Danbooru implementation. The API is largely different (mostly REST URL syntax) but functionality is the same, or even better than the Konachan / yande.re Danbooru.

Supported:

- TODO

Return types:

- JSON
- XML

Structure of a post:

TODO

Structure of a tag:

TODO

Structure of a pool:

TODO



## Gelbooru

Very limited API. Basically only returns posts.

Supported:

- Posts

Return types:

- XML

Structure of a post:

TODO

Structure of a tag:

TODO

Structure of a pool:

TODO



[^1]: The service often returns invalid data via JSON, hence it's more reliable via XML
[^2]: No method for fetching tags in batches
[^3]: Tags are returned in the wrong order via JSON, so it's better to use XML