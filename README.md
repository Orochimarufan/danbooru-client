## Danbooru client

This is a (work in progress) C++ port from the original [Danbooru Client](http://kde-apps.org/content/show.php?content=114343).

Current features:

- Supports most Danbooru image boards
- Search by one or multiple tags
- Filter out posts with unwanted tags
- View or download posts
- QML-based thumbnail view
- Pools

### Requirements

[KDE Frameworks](https://community.kde.org/Frameworks) 5.7 or newer (needed for some features from KDeclarative), and Qt 5.4.

In particular, Danbooru Client makes use of the following Frameworks:

- KIconThemes (icon handling)
- KCoreAddons
- KGuiAddons (cache handling)
- KCompletion (Combo box widgets)
- KXmlGui (main window UI)
- KConfigWidgets (configuration handling)
- KIO (network operation handling)
- KWallet (password handling)
- KDeclarative (thumbnail view)

At minimum, the following Qt 5.4 components are required as well:

- QtCore
- QtWidgets
- QtQuick
- QtQuickWidgets
- QtQml

You will also need [extra-cmake-modules](http://api.kde.org/ecm/index.html) and CMake (at least version 3; lower versions may work but I have not tested them).

### Downloading, building and installing

No releases have been made yet. In the mean time, you can use the source directly off the Git repository.

Checkout the source:

```
git clone https://git.dennogumi.org/kde/danbooru-client.git
```

Move inside the directory, and create a ``build`` directory under it:

```
mkdir build; cd build
```

Configure using CMake and build:

```
cmake -DCMAKE_INSTALL_PREFIX=$(qtpaths --install-prefix) ../
make
```

Then install, either as root or using ``sudo``:

```
make install
```

### Notice

Several Danbooru boards handle their programmatic access ("API") in slightly different ways. Therefore, this program may not work correctly on all of them. yande.re and konachan.{com, net} have been tested successfully.
Notice that Danbooru Client does **not** implement the Gelbooru API at the moment.

### License

This is Free Software, licensed under the GNU GPL v3 or later. See the LICENSE file for more details.