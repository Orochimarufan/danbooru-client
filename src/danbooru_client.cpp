/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include "danbooru_client_debug.h"
#include <QUrl>
#include <QCommandLineParser>

#include <KLocalizedString>
#include <KAboutData>

#include "mainwindow.h"
#include "danbooru_version.h"
#include "libdanbooru/libdanbooru_version.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCoreApplication::setApplicationName(QLatin1String("danbooru-client"));
    QCoreApplication::setApplicationVersion(QLatin1String(DANBOORU_CLIENT_VERSION_STRING));
    QCoreApplication::setOrganizationDomain(QLatin1String("dennogumi.org"));
    QApplication::setApplicationDisplayName(i18n("Danbooru Client"));
    KLocalizedString::setApplicationDomain("danbooru-client");

    KAboutData aboutData(I18N_NOOP(QLatin1String("danbooru-client")),
                         i18n("Danbooru Client"),
                         QLatin1String(DANBOORU_CLIENT_VERSION_STRING),
                         i18n("KF5 based Danbooru client"),
                         KAboutLicense::GPL_V3,
                         i18n("(C) 2015 Luca Beltrame"),
                         QString("Using libdanbooru version %1").arg(LIBDANBOORU_VERSION_STRING),
                         QLatin1String("https://git.dennogumi.org/kde/danbooru-client"),
                         QLatin1String("https://git.dennogumi.org/kde/danbooru-client")
                        );

    aboutData.addAuthor(i18n("Luca Beltrame"), i18n("Developer"),
                        QLatin1String("lbeltrame@kde.org")
                       );

    KAboutData::setApplicationData(aboutData);

    QCommandLineParser cmdParser;
    cmdParser.addHelpOption();
    cmdParser.addVersionOption();
    aboutData.setupCommandLine(&cmdParser);
    cmdParser.process(app);
    aboutData.processCommandLine(&cmdParser);

    Danbooru::DanbooruMainWindow *window = new Danbooru::DanbooruMainWindow();
    window->setObjectName("danbooruMainWindow");
    window->show();

    // Enable HighDPI support
    app.setAttribute(Qt::AA_UseHighDpiPixmaps, true);

    return app.exec();
}