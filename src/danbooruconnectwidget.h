/*
 * This file is part of Danbooru Client.
 * Copyright 2013  Luca Beltrame <lbeltrame@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ui_danbooruconnectwidget.h"

#include <QWidget>
#include <QString>
#include <QVector>

#include <QUrl>

namespace KWallet
{
class Wallet;
}

namespace Danbooru
{

class DanbooruService;

const QMap< QUrl, QString > initBoardSalts();

class DanbooruConnectWidget: public QWidget, public Ui::DanbooruConnectWidget
{
    Q_OBJECT

public:
    explicit DanbooruConnectWidget(QVector<QUrl> urlList,
                                   QWidget *parent = 0);
    ~DanbooruConnectWidget();

    QString username() const;
    QString password() const;
    QUrl boardUrl() const;
    bool isAnonymous() const;
    void setBoards(const QVector<QUrl> &urlList);

private:
    QUrl  m_boardUrl;
    QString m_username;
    QString m_password;
    KWallet::Wallet *m_wallet;
    static const QMap<QUrl, QString> boardSalts;

Q_SIGNALS:
    void connectionEstablished(DanbooruService *service);
    void accepted();
    void rejected();

private Q_SLOTS:
    void checkWallet(bool);
    void getWalletData();
    void toggleLineEdits(int state);
    void accept();
};

} // namespace Danbooru
