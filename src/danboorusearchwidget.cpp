/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#include "danboorusearchwidget.h"

#include <QPushButton>
#include <QLineEdit>

namespace Danbooru
{

DanbooruSearchWidget::DanbooruSearchWidget(QWidget *parent): QWidget(parent)
{
    setupUi(this);

    tagLineEdit->setPlaceholderText(i18n("Type search tags."));
    tagLineEdit->setToolTip(i18n("Type search tags. An empty string searches all posts."));

    connect(searchButton, &QPushButton::clicked, this, &DanbooruSearchWidget::accept);
    connect(tagLineEdit, &QLineEdit::returnPressed, this, &DanbooruSearchWidget::accept);
    connect(closeButton, &QPushButton::clicked, [this]() {
        Q_EMIT(rejected());
    });

}

DanbooruSearchWidget::~DanbooruSearchWidget()
{
}

QStringList DanbooruSearchWidget::selectedTags() const
{
    return m_tags;
}

void DanbooruSearchWidget::accept()
{

    m_tags = tagLineEdit->text().split(",");
    Q_EMIT(accepted());
}

} // namespace Danbooru