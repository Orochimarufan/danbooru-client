/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DANBOORU_SEARCHWIDGET_H
#define DANBOORU_SEARCHWIDGET_H

#include "ui_searchwidget.h"

namespace Danbooru
{

class DanbooruSearchWidget: public QWidget, Ui::SearchWidget
{

    Q_OBJECT

public:
    explicit DanbooruSearchWidget(QWidget *parent = 0);
    ~DanbooruSearchWidget();
    QStringList selectedTags() const;

private:
    QStringList m_tags;

private Q_SLOTS:
    void accept();

Q_SIGNALS:
    void accepted();
    void rejected();

};

} // namespace Danbooru

#endif