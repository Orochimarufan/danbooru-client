/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#include "danboorutagwidget.h"
#include "model/danboorutagmodel.h"
#include "libdanbooru/danboorutag.h"

namespace Danbooru
{

DanbooruTagWidget::DanbooruTagWidget(QWidget *parent): QListView(parent)
{
}

DanbooruTagWidget::~DanbooruTagWidget()
{
}

void DanbooruTagWidget::addTag(DanbooruTag *tag)
{

    if (m_blacklist.contains(tag->name())) {
        return;
    }

    qobject_cast<DanbooruTagModel *>(model())->addTag(tag);

}

void DanbooruTagWidget::setBlackList(const QStringList &blackList)
{
    m_blacklist = blackList;
}

} // namespace Danbooru
