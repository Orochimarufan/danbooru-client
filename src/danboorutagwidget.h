/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DANBOORU_TAGWIDGET_H
#define DANBOORU_TAGWIDGET_H

#include <QListView>
#include <QStringList>

namespace Danbooru
{

class DanbooruTag;

class DanbooruTagWidget: public QListView
{

    Q_OBJECT

private:
    QStringList m_blacklist;

public:
    explicit DanbooruTagWidget(QWidget *parent = 0);
    ~DanbooruTagWidget();
    void setBlackList(const QStringList &blacklist);

public Q_SLOTS:
    void addTag(DanbooruTag *tag);
};
} // namespace Danbooru

#endif
