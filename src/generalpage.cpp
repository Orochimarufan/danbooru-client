/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#include "generalpage.h"

#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QLineEdit>

#include "danboorusettings.h"

static QLatin1String urlRegex("(http|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?");

namespace Danbooru
{

GeneralPage::GeneralPage(DanbooruSettings *preferences, QWidget *parent): QWidget(parent)
{
    setupUi(this);
    kcfg_Boards->insertStringList(preferences->boards());
    QRegularExpression regex(urlRegex);
    QRegularExpressionValidator *validator = new QRegularExpressionValidator(regex);
    kcfg_Boards->lineEdit()->setValidator(validator);
    kcfg_MaxRating->setCurrentIndex(preferences->maxRating());

}

GeneralPage::~GeneralPage()
{
}

} // namespace Danbooru
