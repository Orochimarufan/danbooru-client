/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#include "danboorupool.h"

namespace Danbooru
{

DanbooruPool::DanbooruPool(const QVariantMap &postData, QObject *parent):
    QObject(parent), m_posts(QList<int>())
{
    m_id = postData.value("id").toInt();
    m_name = postData.value("name").toString();
    m_postCount = postData.value("post_count").toInt();
    m_description = postData.value("description").toString();
}

int DanbooruPool::id() const
{
    return m_id;
}

QString DanbooruPool::name() const
{
    return m_name;
}

QString DanbooruPool::description() const
{
    return m_description;
}

int DanbooruPool::postCount() const
{
    return m_postCount;
}

QList< int > DanbooruPool::posts() const
{
    return m_posts;
}

void DanbooruPool::addPost(int post)
{
    m_posts.append(post);
}

void DanbooruPool::addPosts(QList< int > posts)
{
    m_posts.append(posts);
}

void DanbooruPool::addPosts(const QStringList &posts)
{

    for (auto post : posts) {
        m_posts.append(post.toInt());
    }
}

} // namespace Danbooru
