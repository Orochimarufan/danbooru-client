/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DANBOORUPOOL_H
#define DANBOORUPOOL_H

/**
 * @brief This file includes classes which models Danbooru pools.
 * @file danboorupool.h
 *
 **/

// Qt

#include <QObject>
#include <QVariant>
#include <QList>
#include <QStringList>
#include <QXmlStreamAttributes>

namespace Danbooru
{

/**
 * @brief Class representing a Danbooru pool.
 *
 * Pools are organized groups of images, often by a common theme, for
 * example taken from the same artbook. They are identified by unique IDs
 * and are represented by a name, a description, and the posts they
 * contain.
 *
 * @author Luca Beltrame (lbeltrame@kde.org)
 *
 * **/
class DanbooruPool : public QObject
{

    Q_OBJECT

private:
    int m_id;
    int m_postCount;
    QString m_name;
    QString m_description;
    QList<int> m_posts;

public:

    /**
     * @brief Construct a Danbooru pool from a QVariantMap.
     *
     * This form is the easiest to use and should be used when dealing with
     * responses in JSON format. Unfortunately most Danbooru
     * implementations produce broken JSON for some responses.
     *
     * @param postData A QVariantMap from parsed JSON representing the
     *      data from a single pool.
     *
     *
     **/
    DanbooruPool(const QVariantMap &postData, QObject *parent = 0);

    /**
     * @brief Construct a Danbooru pool from a QVariantMap.
     *
     * This form is the easiest to use and should be used when dealing with
     * responses in JSON format. Unfortunately most Danbooru
     * implementations produce broken JSON for some responses.
     *
     * @param postData A QXmlStreamAttributes instance holding the
     *      attributes for the given pool.
     *
     *
     **/
    DanbooruPool(const QXmlStreamAttributes &postData, QObject *parent = 0);

    int id() const;
    int postCount() const;
    QString name() const;
    QString description() const;
    QList<int> posts() const;

    void addPost(int post);
    void addPosts(QList<int> posts);
    void addPosts(const QStringList &posts);
};

} // namespace Danbooru

Q_DECLARE_METATYPE(Danbooru::DanbooruPool *)

#endif // DANBOORUPOOL_H
