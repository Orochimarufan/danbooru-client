/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

// Qt

#include <QtCore/QString>
#include <QtGui/QPixmap>

// KDE

#include <QUrl>

// Own

#include "danboorupost.h"

namespace Danbooru
{

const QMap<QString, DanbooruPost::Rating> DanbooruPost::RATING_MAP = initRatingMap();

DanbooruPost::DanbooruPost(QObject *parent): QObject(parent)
{
    m_pixmap = QPixmap();
    m_tags = QSet<QString>();
    m_height = 0;
    m_width = 0;
    m_url = QUrl();
    m_thumbnailUrl = QUrl();
    m_size = 0;
    m_rating = Questionable;
}

DanbooruPost::DanbooruPost(QVariantMap postData, QPixmap pixmap,
                           QObject *parent):
    QObject(parent),
    m_pixmap(pixmap)
{

    m_tags = QSet<QString>::fromList(postData.value("tags").toString().split(' '));
    m_id = postData.value("id").toString().toInt();
    m_height = postData.value("height").toString().toInt();
    m_width = postData.value("width").toString().toInt();

    m_url = postData.value("file_url").toUrl();
    m_thumbnailUrl = postData.value("preview_url").toUrl();
    m_size = postData.value("file_size").toInt();
    m_rating = RATING_MAP.value(postData.value("rating").toString());
    m_sampleUrl = postData.value("sample_url").toUrl();

}

DanbooruPost::DanbooruPost(QXmlStreamAttributes &postData, QPixmap pixmap, QObject *parent):
    QObject(parent),
    m_pixmap(pixmap)
{

    m_tags = QSet<QString>::fromList(postData.value("tags").toString().split(' '));
    m_id = postData.value("id").toString().toInt();
    m_height = postData.value("height").toString().toInt();
    m_width = postData.value("width").toString().toInt();

    m_url = QUrl::fromUserInput(postData.value("file_url").toString());
    m_thumbnailUrl = QUrl::fromUserInput(postData.value("preview_url").toString());
    m_sampleUrl = QUrl::fromUserInput(postData.value("sample_url").toString());

    m_size = postData.value("file_size").toString().toInt();
    m_rating = RATING_MAP.value(postData.value("rating").toString());
}

DanbooruPost::~DanbooruPost()
{
}

const QMap< QString, DanbooruPost::Rating > DanbooruPost::initRatingMap()
{

    QMap<QString, DanbooruPost::Rating> map;
    map.insert("s", Safe);
    map.insert("q", Questionable);
    map.insert("e", Explicit);

    return map;

}

bool DanbooruPost::operator==(const Danbooru::DanbooruPost &other)
{
    return m_url == other.m_url && m_id == other.m_id;
}

void DanbooruPost::setPixmap(const QPixmap &pixmap)
{
    m_pixmap = pixmap;
}

const QString DanbooruPost::toString()
{
    QString display = QString("Danbooru Post with ID %1 and URL %2, width %3 height %4 ");
    display = display.arg(id()).arg(fileUrl().url()).arg(width()).arg(height());
    return display;
}

int DanbooruPost::id() const
{
    return m_id;
}

int DanbooruPost::height() const
{
    return m_height;
}

int DanbooruPost::width() const
{
    return m_width;
}

const QSet< QString > DanbooruPost::tags() const
{
    return m_tags;
}

const QUrl DanbooruPost::thumbnailUrl() const
{
    return m_thumbnailUrl;
}

const QUrl DanbooruPost::sampleUrl() const
{
    return m_sampleUrl;
}

QPixmap DanbooruPost::pixmap() const
{
    return m_pixmap;
}

int DanbooruPost::size() const
{
    return m_size;
}

const QUrl DanbooruPost::fileUrl() const
{
    return m_url;
}

Danbooru::DanbooruPost::Rating DanbooruPost::rating() const
{
    return m_rating;
}

} // namespace Danbooru

