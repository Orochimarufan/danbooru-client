/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#include "danboorutag.h"

namespace Danbooru
{

DanbooruTag::DanbooruTag(const QVariantMap &postData, QObject *parent):
    QObject(parent)
{
    m_id = postData.value("id").toInt();
    m_name = postData.value("name").toString();
    m_count = postData.value("count").toInt();
    m_ambiguous = postData.value("ambiguous").toBool();

    int type = postData.value("type").toInt();

    switch (type) {
    case 0:
        m_tagType = General;
        break;
    case 1:
        m_tagType = Artist;
        break;
    case 2:
        m_tagType = Copyright;
        break;
    case 3:
        m_tagType = Character;
        break;
    case 4:
        m_tagType = Unknown;
        break;
    default:
        m_tagType = Unknown;
        break;
    }
}

DanbooruTag::~DanbooruTag()
{
}

int DanbooruTag::id() const
{
    return m_id;
}

int DanbooruTag::count() const
{
    return m_count;
}

const QString DanbooruTag::name() const
{
    return m_name;
}

bool DanbooruTag::ambiguous() const
{
    return m_ambiguous;
}

Danbooru::DanbooruTag::TagType DanbooruTag::type() const
{
    return m_tagType;
}

} // namespace Danbooru
