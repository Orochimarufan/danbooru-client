/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DANBOORUTAG_H
#define DANBOORUTAG_H

#include <QtCore/QObject>
#include <QtCore/QVariant>

#include "danbooru.h"

namespace Danbooru
{

class DanbooruTag : public QObject
{
    Q_OBJECT

public:
    /**
    * @brief Types of tags
    *
    * A Danbooru tag is not simply a string, but carries some (limited)
    * semantic information. In particular, tags are organized in what they
    * refer to, either something related to the image itself, or to the
    * artist that drew it, or the copyrights associated to the image, or even
    * the characters that are represented in it.
    *
    **/

    enum TagType {
        General = 1, /**< Generic tags **/
        Artist = 2, /**< Tags related to artists **/
        Copyright = 4, /**<Tags related to copyrights **/
        Character = 8, /**<Tags related to characters **/
        Unknown = 16 /**< Unknown tags **/
    };

private:
    int m_id;
    int m_count;
    QString m_name;
    bool m_ambiguous;
    TagType m_tagType;

public:
    DanbooruTag(const QVariantMap &postData, QObject *parent = 0);
    ~DanbooruTag();
    int id() const;
    int count() const;
    const QString name() const;
    bool ambiguous() const;
    TagType type() const;

};

Q_DECLARE_FLAGS(TagTypes, DanbooruTag::TagType)
Q_DECLARE_OPERATORS_FOR_FLAGS(TagTypes)

} // namespace  Danbooru

Q_DECLARE_METATYPE(Danbooru::DanbooruTag *)

#endif // DANBOORUTAG_H
