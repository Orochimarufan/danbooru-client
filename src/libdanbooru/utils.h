/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H
#define UTILS_H

#include "danboorupost.h"

// Qt

#include <QtCore/QStringList>

// KDE

#include <QUrl>

/**
 * @brief Commmon utilities for Danbooru classes.
 * @file utils.h
 *
 **/

namespace Danbooru
{

typedef QMap<QString, QString> dictMap;

/** @brief Generate a request URL for a Danbooru board.
 *
 * Given an URL and an API path, this function builds a
 * proper URL which is used for the specific API operation.
 * The processing follows the "normal" way of encoding URLs.
 *
 * @param url The board URL.
 * @param path The API path of the call to use
 * @param username The username to supply (optional)
 * @param password The password to use (optional)
 * @param parameters A map of key,values representing the parameters
 *                   to use.
 * @param tags The tags to supply (optional).
 *
 * @return A constructed URL to be used for a Danbooru API call.
 * @author Luca Beltrame (lbeltrame@kde.org)
 *
 *
 **/
QUrl requestUrl(QUrl &url, const QString &path, const QString &username,
                const QString &password, const dictMap &parameters,
                const QStringList &tags);

/** @brief Generate a request URL for a Danbooru board.
 *
 * This is an overloaded function provided for convenience.
 *
 *
 * @param url The board URL.
 * @param path The API path of the call to use
 * @param username The username to supply (optional)
 * @param password The password to use (optional)
 * @param parameters A map of key,values representing the parameters
 *                   to use.
 *
 * @return A constructed URL to be used for a Danbooru API call.
 * @author Luca Beltrame (lbeltrame@kde.org)
 *
 *
 **/
QUrl requestUrl(QUrl &url, const QString &path, const QString &username,
                const QString &password, const dictMap &parameters);

/** @brief Generate a request URL for a Danbooru board.
 *
 * This is an overloaded function provided for convenience.
 *
 * @param url The board URL.
 * @param path The API path of the call to use
 * @param username The username to supply (optional)
 * @param password The password to use (optional)
 *
 * @return A constructed URL to be used for a Danbooru API call.
 * @author Luca Beltrame (lbeltrame@kde.org)
 *
 *
 **/
QUrl requestUrl(QUrl &url, const QString &path, const QString &username,
                const QString &password);

QList<QVariant> parseDanbooruResult(QByteArray data, QString xlmElement,
                                    bool *result);
QVariant parseDanbooruResult(QByteArray data, bool *result);

/**
 * @brief Check if a post can be allowed.
 *
 * This convenience function checks if a DanbooruPost is not allowed, either because it
 * contains blacklisted tags, or because it has a higher rating than allowed.
 *
 * @param post A DanbooruPost pointer.
 * @param blacklist A QSet containing unwanted tag names as QStrings.
 * @param maxRating The maximum allowed rating expressed as a DanbooruPost::Ratings flag.
 * @return true if the post is unwanted, false otherwise.
 * @author Luca Beltrame (lbeltrame@kde.org)
 */
bool isPostBlacklisted(DanbooruPost *post, QSet<QString> blacklist, DanbooruPost::Ratings maxRating);

}
#endif // UTILS_H
