/*
 * This file is part of Danbooru Client.
 * Copyright 2013  Luca Beltrame <lbeltrame@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef DANBOORU_CLIENT_H
#define DANBOORU_CLIENT_H

#include "libdanbooru/danboorupost.h"

#include <QTableView>
#include <QListView>

#include <kxmlguiwindow.h>
#include <KSharedDataCache>
#include <KImageCache>

class QQuickWidget;
class QSortFilterProxyModel;

namespace Danbooru
{

class DanbooruClientView;
class DanbooruService;
class DanbooruPostModel;
class DanbooruPoolModel;
class DanbooruTagModel;
class DanbooruConnectWidget;
class DanbooruSearchWidget;
class DanbooruTagWidget;

/**
 * This class serves as the main window for danbooru_client.  It handles the
 * menus, toolbars and status bars.
 *
 * @short Main window class
 * @author Luca Beltrame <lbeltrame@kde.org>
 * @version 0.01
 */
class DanbooruMainWindow : public KXmlGuiWindow
{
    Q_OBJECT

private:
    QQuickWidget *m_view;
    DanbooruPostModel *m_model;
    DanbooruPoolModel *m_poolModel;
    DanbooruTagModel *m_tagModel;
    DanbooruService *m_service;
    DanbooruConnectWidget *m_connectWidget;
    DanbooruSearchWidget *m_searchWidget;
    DanbooruTagWidget *m_tagWidget;
    QSortFilterProxyModel *m_proxyModel;

    QTableView *m_tableView;
    KImageCache *m_cache;
    static QHash<int, DanbooruPost::Rating> ratingMap;

public:
    /**
     * Default Constructor
     */
    DanbooruMainWindow(QWidget *parent = 0);

    /**
     * Default Destructor
     */
    virtual ~DanbooruMainWindow();

private:
    void setupActions();
    void setupDockWidgets();
    void setupConnections();
    void clearModels();
    void handlePostDownload(const QStringList &tags = QStringList(), bool relatedTags = false);

private Q_SLOTS:
    void connectToBoard();
    void downloadPosts();
    void optionsPreferences();
    void loadSettings();
    void slotHandleDownload(const QUrl &url, QVariant tags);
    void searchTag(const QModelIndex &index);

};
} // namespace Danbooru

#endif // _DANBOORU_CLIENT_H_
