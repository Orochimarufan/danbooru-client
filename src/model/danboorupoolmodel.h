/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DANBOORU_POOLMODEL_H
#define DANBOORU_POOLMODEL_H

#include <QVector>
#include <QAbstractTableModel>

namespace Danbooru
{

class DanbooruPool;

class DanbooruPoolModel: public QAbstractTableModel
{
    Q_OBJECT

public:
    DanbooruPoolModel(QObject *parent = 0);
    ~DanbooruPoolModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const Q_DECL_OVERRIDE;
    void clear();
    DanbooruPool *poolAt(int index) const;

private:
    QVector<DanbooruPool *> m_items;
    static const QStringList m_headerNames;

public Q_SLOTS:
    void addPool(Danbooru::DanbooruPool *pool);

};

} // namespace Danbooru

#endif