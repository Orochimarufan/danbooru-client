/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

// Own

#include "danboorupostmodel.h"
#include "libdanbooru/danboorupost.h"

// Qt

#include <QPixmap>

namespace Danbooru
{

DanbooruPostModel::DanbooruPostModel(QObject *parent): QAbstractListModel(parent)
{

}

DanbooruPostModel::~DanbooruPostModel()
{

    if (!m_items.isEmpty()) {
        qDeleteAll(m_items);
        m_items.clear();
    }
}

int DanbooruPostModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_items.size();
}

void DanbooruPostModel::addPost(Danbooru::DanbooruPost *post)
{

    beginInsertRows(QModelIndex(), m_items.size(), m_items.size());
    m_items.append(post);
    m_postTags.unite(post->tags());
    endInsertRows();
}

QVariant DanbooruPostModel::data(const QModelIndex &index, int role) const
{

    if (!index.isValid()) {
        return QVariant();
    }

    if (index.row() >= m_items.size() || index.row() < 0) {
        return QVariant();
    }

    if (m_items.isEmpty()) {
        return QVariant();
    }

    DanbooruPost *post = m_items.at(index.row());

    if (!post) {
        return QVariant();
    }

    switch (role) {
    case Qt::DisplayRole: {
        QVariant variant;
        variant.setValue(post);

        return variant;
    }

    case Qt::DecorationRole: {
        const QPixmap pixmap = post->pixmap();
        return pixmap;
    }
    case Qt::ToolTipRole:
        return post->fileUrl().fileName();
    case FileUrlRole:
        return post->fileUrl();
    case ThumbUrlRole:
        return post->thumbnailUrl();
    case SizeRole:
        return post->size();
    case ResolutionRole:
        return QSize(post->width(), post->height());
    case RatingRole:
        return post->rating();
    case TagRole: {
        QStringList tagList = post->tags().toList();
        return tagList;
    }
    case SampleUrlRole:
        return post->sampleUrl();
    default:
        return QVariant();
    }

    return QVariant();
}

void DanbooruPostModel::clear()
{

    if (m_items.isEmpty()) {
        return;
    }

    beginResetModel();
    qDeleteAll(m_items);
    m_items.clear();
    endResetModel();
}

QHash< int, QByteArray > DanbooruPostModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Qt::DecorationRole] = "thumbPix";
    roles[FileUrlRole] = "fileUrl";
    roles[ThumbUrlRole] = "thumbnailUrl";
    roles[RatingRole] = "rating";
    roles[TagRole] = "tags";
    roles[SizeRole] = "fileSize";
    roles[ResolutionRole] = "resolution";
    roles[SampleUrlRole] = "sampleUrl";
    return roles;
}

QStringList DanbooruPostModel::postTags() const
{

    return m_postTags.toList();

}

} // namespace Danbooru
