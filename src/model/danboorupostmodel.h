/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DANBOORUPOSTMODEL_H
#define DANBOORUPOSTMODEL_H

/**
 * @brief This file contains a specific model to represent Danbooru Items
 * @file danboorutablemodel.h
 *
 **/

#include <QAbstractListModel>
#include <QVector>
#include <QSet>

namespace Danbooru
{

class DanbooruPost;

/**
 * @brief A model to represent DanbooruItems
 *
 * Since items from a Danbooru service are sent by the service itself,
 * there is no need for sorting or table-like structures: everything is
 * represented as a flat list.
 *
 * Items are added through the addPost() slot.
 *
 */
class DanbooruPostModel : public QAbstractListModel
{

    Q_OBJECT

public:
    DanbooruPostModel(QObject *parent = 0);
    ~DanbooruPostModel();

    Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    void clear();
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    QStringList postTags() const;

    enum PostRoles {
        FileUrlRole = Qt::UserRole + 1001,
        ThumbUrlRole = Qt::UserRole + 1002,
        SizeRole = Qt::UserRole + 1003,
        RatingRole = Qt::UserRole + 1004,
        TagRole = Qt::UserRole + 1005,
        ResolutionRole = Qt::UserRole + 1006,
        SampleUrlRole = Qt::UserRole + 1007
    };

private:
    QVector<DanbooruPost *> m_items;
    QSet<QString> m_postTags;

public Q_SLOTS:
    /**
     * @brief Add a new post to the model
     *
     * Connect to this slot when you want to add items to the model.
     *
     * @param post A pointer to a DanbooruPost.
     *
     */
    void addPost(Danbooru::DanbooruPost *post);

};
} // namespace Danbooru

#endif // DANBOORUPOSTMODEL_H
