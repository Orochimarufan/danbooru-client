/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#include "danboorutagmodel.h"
#include "libdanbooru/danboorutag.h"
#include "danbooru_client_debug.h"

#include <KLocalizedString>

namespace Danbooru
{

DanbooruTagModel::DanbooruTagModel(QObject *parent): QAbstractListModel(parent)
{
}

DanbooruTagModel::~DanbooruTagModel()
{
    if (!m_items.isEmpty()) {
        qDeleteAll(m_items);
        m_items.clear();
    }
}

int DanbooruTagModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_items.size();
}

QVariant DanbooruTagModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    DanbooruTag *tag = m_items.at(index.row());

    if (role == Qt::DisplayRole) {
        return i18np("%1 (%2 post)", "%1 (%2 posts)", tag->name(), tag->count());
    } else if (role == Qt::ToolTipRole) {
        return i18n("Tag count: %1", tag->count());
    } else if (role == TagCountRole) {
        return tag->count();
    }

    // TODO: More roles depending on the type of information

    return QVariant();
}

DanbooruTag *DanbooruTagModel::itemAt(int index) const
{
    return m_items.at(index);
}

void DanbooruTagModel::addTag(DanbooruTag *tag)
{
    if (!tag) {
        return;
    }

    if (tag->count() == 0) {
        return;
    }

    beginInsertRows(QModelIndex(), m_items.size(), m_items.size());
    m_items.append(tag);
    endInsertRows();
}

void DanbooruTagModel::clear()
{

    if (m_items.isEmpty()) {
        return;
    }

    beginResetModel();
    qDeleteAll(m_items);
    m_items.clear();
    endResetModel();
}

} // namespace Danbooru