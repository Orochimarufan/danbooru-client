/*
 * Copyright 2015 Luca Beltrame <lbeltrame@kde.org>
 *
 * This file is part of Danbooru Client.
 *
 * Danbooru Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Danbooru Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Danbooru Client. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DANBOORU_TAGMODEL_H
#define DANBOORU_TAGMODEL_H

#include <QAbstractListModel>
#include <QVector>

namespace Danbooru
{

class DanbooruTag;

class DanbooruTagModel: public QAbstractListModel
{

    Q_OBJECT

public:
    explicit DanbooruTagModel(QObject *parent = 0);
    ~DanbooruTagModel();
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    void clear();
    DanbooruTag *itemAt(int index) const;

    enum TagRoles {
        // Needed for sorting through the proxy model
        TagCountRole = Qt::UserRole + 4000
    };

private:
    QVector<DanbooruTag *> m_items;

public Q_SLOTS:
    void addTag(Danbooru::DanbooruTag *tag);

};

} // namespace Danbooru

#endif